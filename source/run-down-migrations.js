'use strict'

const { database, down } = require('migrate-mongo')
const R = require('ramda')
require('log-timestamp')

database
  .connect()
  .then(({ db, client }) => down(db, client))
  .then(R.forEach(fileName => console.info(`Migrated Down: ${fileName}`)))
  .then(() => process.exit(0))
  .catch(error => {
    console.error(`Error running Down Migrations`)
    console.error(error)
    process.exit(1)
  })
