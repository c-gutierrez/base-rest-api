const jwt = require('jsonwebtoken')
const passport = require('passport')
const { completeResponse, completeErrorResponse } = require('../fn/controllerFn')

/**
 * Run local auth strategy for passport lib
 * @param {Object} req
 * @param {Object} res
 */
const localAuth = strategyFn => (req, res, next) => {
  strategyFn() // Load local strategy

  passport.authenticate('local', { session: false }, (err, user, info) => {
    const manageResponse = completeResponse(res)

    if (err || !user) {
      return manageResponse(401)({
        message: info ? info.message : 'Unauthorized',
        user: user,
      })
    }

    req.login(user, { session: false }, err => {
      if (err) {
        return completeErrorResponse(res, 500, err)
      }

      const token = jwt.sign(user, 'your_jwt_secret')

      return manageResponse(200)({ user, token })
    })
  })(req, res)
}

/**
 * Run jwt auth strategy for passport lib
 * @param {Object} req
 * @param {Object} res
 */
const jwtAuth = strategyFn => (req, res, next) => {
  strategyFn() // Load jwt strategy
  return passport.authenticate('jwt', { session: false })(req, res, next)
}

module.exports = { localAuth, jwtAuth }
