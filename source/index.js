const express = require('express')
const R = require('ramda')
const config = require('./config')
const dbFn = require('./fn/dbFn')

// Init express app
const app = express()
const port = 3000

// Establish DB connection
dbFn.connect(R.propOr({}, ['db'], config))

// Parse request body
app.use(express.json())

// Home routes
app.get('/', (req, res) => res.send('Hello World!'))

// Users routes
const usersRouter = require('./users/router')
app.use('/users', usersRouter)

// Customers routes
const customersRouter = require('./customers/router')
app.use('/customers', customersRouter)

// Recipients routes
const recipientsRouter = require('./recipients/router')
app.use('/customers/:customerId/recipients', recipientsRouter)

// Messages routes
const messagesRouter = require('./messages/router')
app.use('/customers/:customerId/messages', messagesRouter)

// Launch express server
app.listen(port, () => console.info(`Example app listening on port ${port}!`))
