module.exports = {
  root: true,
  extends: ['eslint:recommended', 'standard', 'prettier', 'prettier/standard'],
  globals: {
    requireLib: true,
    __basedir: true,
  },
  rules: {
    'no-var': 2,
    'prefer-const': 2,
    'no-debugger': 2,
    'no-console': [2, { allow: ['info', 'error'] }],
    'new-cap': 0,
    'no-unused-expressions': 0,
    'no-sequences': 0,
  },
}
