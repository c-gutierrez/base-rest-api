'use strict'

const { database, up } = require('migrate-mongo')
const R = require('ramda')
require('log-timestamp')

database
  .connect()
  .then(({ db, client }) => up(db, client))
  .then(R.forEach(fileName => console.info(`Migrated Up: ${fileName}`)))
  .then(() => process.exit(0))
  .catch(error => {
    console.error(`Error running Up Migrations`)
    console.error(error)
    process.exit(1)
  })
