'use strict'

const schema = {
  properties: {
    name: { type: 'string' },
    lastname: { type: 'string' },
    email: { type: 'string', format: 'email' },
    password: { type: 'string' },
  },
}

module.exports = schema
