'use strict'

const R = require('ramda')
const express = require('express')
const userRouter = express.Router()
const { assignMiddlewaresToRouter, createBasicRoutes } = require('../fn/routerFn')
const { errorHandler } = require('../fn/controllerFn')
const { userControllerFn } = require('./controller')
const { extendedRepositoryFn } = require('../fn/repositoryFn')
const {
  validateSchemaAgainstBody,
  validateSchemaMongooseAgainstBody,
} = require('../fn/middlewareFn')
const buildControllerAndRepoByModel = R.compose(userControllerFn, extendedRepositoryFn)
const usersModel = require('./model')
const userController = buildControllerAndRepoByModel(usersModel)
const usersSchema = require('./schema')
const usersMongooseSchema = require('./mongooseSchema')
const { localAuth, jwtAuth } = require('../passport/auth')
const localStrategyFn = require('./localStrategy')
const jwtStrategyFn = require('./jwtStrategy')

const validateUserSchemaMiddleware = validateSchemaAgainstBody(usersSchema)
const validateUserMongooseSchemaMiddleware = validateSchemaMongooseAgainstBody(usersMongooseSchema)

userRouter.post('/login', localAuth(localStrategyFn))
userRouter.post(
  '/register',
  [validateUserSchemaMiddleware, validateUserMongooseSchemaMiddleware],
  userController.register
)

assignMiddlewaresToRouter(userRouter, [jwtAuth(jwtStrategyFn)])
userRouter.post(
  '/',
  [validateUserSchemaMiddleware, validateUserMongooseSchemaMiddleware],
  userController.register
)

createBasicRoutes(userRouter, userController)

// Error handler
userRouter.use(errorHandler)

module.exports = userRouter
