const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const usersModel = require('./model')
const { retrieveOneFn } = require('../fn/repositoryFn')
const R = require('ramda')
const { mapUserToLogin } = require('./fn')
const { toObject } = require('../fn/helperFn')
const { compareEncryptValue } = require('../fn/encryptionFn')

/**
 * Load local strategy for passport
 */
const localStrategyFn = () => {
  const validateUserPassword = plainPassword =>
    R.compose(compareEncryptValue(plainPassword), R.propOr('', ['password']))

  const validateUser = R.curry((cb, password, user) =>
    validateUserPassword(password)(user).then(isValidPassword => {
      if (!isValidPassword) {
        return cb(null, false, { message: 'Incorrect email or password.' })
      }
      return cb(null, mapUserToLogin(user), { message: 'Logged In Successfully' })
    })
  )

  passport.use(
    new LocalStrategy(
      {
        usernameField: 'email',
        passwordField: 'password',
      },
      (email, password, cb) => {
        return retrieveOneFn(usersModel)({ email })
          .then(R.compose(validateUser(cb, password), toObject))
          .catch(cb)
      }
    )
  )
}

module.exports = localStrategyFn
