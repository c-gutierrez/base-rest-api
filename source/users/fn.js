const R = require('ramda')

/**
 * Map user data to be shown after successfull login
 * @param {*} user
 * @returns Object
 */
const mapUserToLogin = user => ({
  _id: R.propOr('', ['_id'], user),
  name: R.propOr('', ['name'], user),
  lastname: R.propOr('', ['lastname'], user),
  email: R.propOr('', ['email'], user),
})

module.exports = { mapUserToLogin }
