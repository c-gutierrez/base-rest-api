const passport = require('passport')
const passportJWT = require('passport-jwt')
const ExtractJWT = passportJWT.ExtractJwt
const JWTStrategy = passportJWT.Strategy
const usersModel = require('./model')
const { retrieveByIdFn } = require('../fn/repositoryFn')
const R = require('ramda')
const { mapUserToLogin } = require('./fn')
const { toObject } = require('../fn/helperFn')

/**
 * Load jwt strategy for passport
 */
const jwtStrategyFn = () => {
  const validateUser = user => {
    if (!user || R.isEmpty(user)) {
      return false
    }
    return mapUserToLogin(user)
  }

  passport.use(
    new JWTStrategy(
      {
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
        secretOrKey: 'your_jwt_secret',
      },
      (jwtPayload, cb) => {
        return retrieveByIdFn(usersModel)(R.propOr('', ['_id'], jwtPayload))
          .then(R.compose(validateUser, toObject))
          .then(user => cb(null, user))
          .catch(cb)
      }
    )
  )
}

module.exports = jwtStrategyFn
