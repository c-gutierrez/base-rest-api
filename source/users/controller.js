const R = require('ramda')
const { completeErrorResponse, extendedControllerFn } = require('../fn/controllerFn')
const { localAuth } = require('../passport/auth')
const localStrategyFn = require('./localStrategy')
const { encryptValue } = require('../fn/encryptionFn')

/**
 * Manage register request to create a new user a return access token
 * @param {Object} repository
 * @param {Object} req
 * @param {Object} res
 */
const registerFn = repository => (req, res) => {
  const data = R.propOr([], 'body', req)

  const plainPassword = R.propOr('', ['password'], data)

  encryptValue(plainPassword).then(encryptedPassword => {
    const newData = Object.assign(data, { password: encryptedPassword })

    repository
      .create(newData)
      .then(user => {
        req.body = {
          email: R.propOr('', ['email'], user),
          password: plainPassword,
        }

        localAuth(localStrategyFn)(req, res)
      })
      .catch(completeErrorResponse(res, 500))
  })
}

/**
 * Create a new object with the crud and extra functions
 * @param {Object} repository
 * @returns {}
 */
const userControllerFn = repository => {
  const controller = extendedControllerFn(repository)
  const extraFunctions = { register: registerFn(repository) }

  return Object.assign(controller, extraFunctions)
}

module.exports = {
  registerFn,
  userControllerFn,
}
