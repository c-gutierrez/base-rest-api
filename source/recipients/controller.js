'use strict'

const { extendedControllerFn } = require('../fn/controllerFn')
/**
 * Create a new object with the crud and extra functions
 * @param {Object} repository
 * @returns {}
 */
const recipientControllerFn = repository => {
  const controller = extendedControllerFn(repository)
  const extraFunctions = {}

  return Object.assign(controller, extraFunctions)
}

module.exports = { recipientControllerFn }
