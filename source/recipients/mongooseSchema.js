'use strict'

const mongoose = require('mongoose')
const { isEmail } = require('validator')

const schema = {
  customerId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'customers',
    index: true,
  },
  name: {
    type: String,
    required: true,
  },
  relationship: String,
  contactInfo: {
    email: {
      type: String,
      validate: {
        validator: isEmail,
        message: '{VALUE} is not a valid email',
      },
    },
    mobile: String,
  },
}

module.exports = schema
