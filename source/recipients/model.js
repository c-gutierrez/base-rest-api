'use strict'

const { createSchemaObject, createModelBySchemaObject } = require('../fn/mongooseModelFn')
const schema = require('./mongooseSchema')
const schemeObj = createSchemaObject(schema)

// Add contact info wildcard index, this is not posible at schema level
schemeObj.index({ 'contactInfo.$**': 1 })

module.exports = createModelBySchemaObject('recipients', schemeObj)
