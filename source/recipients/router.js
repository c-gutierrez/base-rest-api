'use strict'

const R = require('ramda')
const express = require('express')
const recipientRouter = express.Router({ mergeParams: true })
const { assignMiddlewaresToRouter, createBasicRoutes } = require('../fn/routerFn')
const { errorHandler } = require('../fn/controllerFn')
const { recipientControllerFn } = require('./controller')
const { extendedRepositoryFn } = require('../fn/repositoryFn')
const { validateSchemaMongooseAgainstBody } = require('../fn/middlewareFn')
const {
  validateCustomerIdInToken,
  injectCustomerIdInBodyFromParam,
} = require('../customers/middlewareFn')
const buildControllerAndRepoByModel = R.compose(recipientControllerFn, extendedRepositoryFn)
const recipientModel = require('./model')
const recipientController = buildControllerAndRepoByModel(recipientModel)
const recipientMongooseSchema = require('./mongooseSchema')
const validateRecipientMongooseSchemaMiddleware = validateSchemaMongooseAgainstBody(
  recipientMongooseSchema
)
const { jwtAuth } = require('../passport/auth')
const jwtStrategyFn = require('../customers/jwtStrategy')

assignMiddlewaresToRouter(recipientRouter, [
  jwtAuth(jwtStrategyFn),
  validateCustomerIdInToken,
  injectCustomerIdInBodyFromParam,
])

recipientRouter.get('/', recipientController.getAllByFieldInParams('customerId'))
recipientRouter.post('/', [validateRecipientMongooseSchemaMiddleware])
recipientRouter.put('/:id', [validateRecipientMongooseSchemaMiddleware])

createBasicRoutes(recipientRouter, recipientController)

// Error handler
recipientRouter.use(errorHandler)

module.exports = recipientRouter
