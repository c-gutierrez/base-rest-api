'use strict'

const { isEmail } = require('validator')

const schema = {
  name: String,
  lastname: String,
  email: {
    type: String,
    required: true,
    unique: true,
    sparse: true,
    validate: {
      validator: isEmail,
      message: '{VALUE} is not a valid email',
    },
  },
  password: {
    type: String,
    required: true,
  },
}

module.exports = schema
