const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const customersModel = require('./model')
const { retrieveOneFn } = require('../fn/repositoryFn')
const R = require('ramda')
const { mapCustomerToLogin } = require('./fn')
const { toObject } = require('../fn/helperFn')
const { compareEncryptValue } = require('../fn/encryptionFn')

/**
 * Load local strategy for passport
 */
const localStrategyFn = () => {
  const validateUserPassword = plainPassword =>
    R.compose(compareEncryptValue(plainPassword), R.propOr('', ['password']))

  const validateCustomer = R.curry((cb, password, customer) =>
    validateUserPassword(password)(customer).then(isValidPassword => {
      if (!isValidPassword) {
        return cb(null, false, { message: 'Incorrect email or password.' })
      }
      return cb(null, mapCustomerToLogin(customer), { message: 'Logged In Successfully' })
    })
  )

  passport.use(
    new LocalStrategy(
      {
        usernameField: 'email',
        passwordField: 'password',
      },
      (email, password, cb) =>
        retrieveOneFn(customersModel)({ email })
          .then(R.compose(validateCustomer(cb, password), toObject))
          .catch(cb)
    )
  )
}

module.exports = localStrategyFn
