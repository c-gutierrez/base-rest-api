'use strict'

const R = require('ramda')
const {
  completeResponse,
  completeErrorResponse,
  manageDatabasePromise,
  extendedControllerFn,
} = require('../fn/controllerFn')
const { localAuth } = require('../passport/auth')
const localStrategyFn = require('./localStrategy')
const { mapCustomerToLogin } = require('./fn')
const { encryptValue } = require('../fn/encryptionFn')

/**
 * Manage register request to create a new user a return access token
 * @param {Object} repository
 * @param {Object} req
 * @param {Object} res
 */
const registerFn = repository => (req, res) => {
  const data = R.propOr([], 'body', req)

  const plainPassword = R.propOr('', ['password'], data)

  encryptValue(plainPassword).then(encryptedPassword => {
    const newData = Object.assign(data, { password: encryptedPassword })

    repository
      .create(newData)
      .then(customer => {
        req.body = {
          email: R.propOr('', ['email'], customer),
          password: plainPassword,
        }

        localAuth(localStrategyFn)(req, res)
      })
      .catch(completeErrorResponse(res, 500))
  })
}

/**
 * Retrieve all records and send them as http response
 * @param {Object} repository
 * @param {Object} req
 * @param {Object} res
 * @returns Promise
 */
const getAll = R.curry((repository, req, res) =>
  manageDatabasePromise(
    repository.retrieve().then(R.map(mapCustomerToLogin)),
    completeResponse(res, 200),
    completeErrorResponse(res, 500)
  )
)

/**
 * Retrieve a customer by id from the entity
 * @param {Object} repository
 * @param {Object} req
 * @param {Object} res
 * @returns Promise
 */
const getOneByIdParam = R.curry((repository, req, res) => {
  const id = R.pathOr('', ['params', 'id'], req)
  return manageDatabasePromise(
    repository.retrieveOne({ _id: id }).then(mapCustomerToLogin),
    completeResponse(res, 200),
    completeErrorResponse(res, 500)
  )
})

/**
 * Create a new object with the crud and extra functions
 * @param {Object} repository
 * @returns {}
 */
const customerControllerFn = repository => {
  const controller = extendedControllerFn(repository)
  const extraFunctions = {
    register: registerFn(repository),
    getAll: getAll(repository),
    getOneByIdParam: getOneByIdParam(repository),
  }

  return Object.assign(controller, extraFunctions)
}

module.exports = {
  registerFn,
  customerControllerFn,
}
