const passport = require('passport')
const passportJWT = require('passport-jwt')
const ExtractJWT = passportJWT.ExtractJwt
const JWTStrategy = passportJWT.Strategy
const customersModel = require('./model')
const { retrieveByIdFn } = require('../fn/repositoryFn')
const R = require('ramda')
const { mapCustomerToLogin } = require('./fn')
const { toObject } = require('../fn/helperFn')

/**
 * Load jwt strategy for passport
 */
const jwtStrategyFn = () => {
  const validateCustomer = customer => {
    if (!customer || R.isEmpty(customer)) {
      return false
    }
    return mapCustomerToLogin(customer)
  }

  passport.use(
    new JWTStrategy(
      {
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
        secretOrKey: 'your_jwt_secret',
      },
      (jwtPayload, cb) => {
        return retrieveByIdFn(customersModel)(R.propOr('', ['_id'], jwtPayload))
          .then(R.compose(validateCustomer, toObject))
          .then(customer => cb(null, customer))
          .catch(cb)
      }
    )
  )
}

module.exports = jwtStrategyFn
