const R = require('ramda')

/**
 * Validate id in token against logged users
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 * @returns next
 */
const validateCustomerIdInToken = R.curry((req, res, next) => {
  const paramId = R.pathOr('', ['params', 'customerId'], req)
  const customerId = R.pathOr('', ['user', '_id'], req)

  if (!R.equals(paramId, customerId.toString())) {
    return res.status(401).send('Unauthorized')
  }

  return next(null)
})

/**
 * Take customerId from session and inject it to body request
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 * @returns next
 */
const injectCustomerIdInBodyFromSession = R.curry((req, res, next) => {
  req.body.customerId = R.pathOr('', ['user', '_id'], req)
  return next(null)
})

/**
 * Take customerId from params and inject it to body request
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 * @returns next
 */
const injectCustomerIdInBodyFromParam = R.curry((req, res, next) => {
  req.body.customerId = R.pathOr('', ['params', 'customerId'], req)
  return next(null)
})

module.exports = {
  validateCustomerIdInToken,
  injectCustomerIdInBodyFromSession,
  injectCustomerIdInBodyFromParam,
}
