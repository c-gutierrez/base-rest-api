'use strict'

const R = require('ramda')
const express = require('express')
const customerRouter = express.Router()
const { errorHandler } = require('../fn/controllerFn')
const { customerControllerFn } = require('./controller')
const { extendedRepositoryFn } = require('../fn/repositoryFn')
const {
  validateSchemaAgainstBody,
  validateSchemaMongooseAgainstBody,
} = require('../fn/middlewareFn')
const { validateCustomerIdInToken } = require('./middlewareFn')
const buildControllerAndRepoByModel = R.compose(customerControllerFn, extendedRepositoryFn)
const customerModel = require('./model')
const customerController = buildControllerAndRepoByModel(customerModel)
const customersSchema = require('./schema')
const customersMongooseSchema = require('./mongooseSchema')
const { localAuth, jwtAuth } = require('../passport/auth')
const localStrategyFn = require('./localStrategy')
const jwtStrategyFn = require('./jwtStrategy')
const userJwtStrategyFn = require('../users/jwtStrategy')
const validateUserSchemaMiddleware = validateSchemaAgainstBody(customersSchema)
const validateUserMongooseSchemaMiddleware = validateSchemaMongooseAgainstBody(
  customersMongooseSchema
)

customerRouter.get('/', jwtAuth(userJwtStrategyFn), customerController.getAll)
customerRouter.post('/', jwtAuth(userJwtStrategyFn), customerController.register)

customerRouter.post('/login', localAuth(localStrategyFn))
customerRouter.post(
  '/register',
  [validateUserSchemaMiddleware, validateUserMongooseSchemaMiddleware],
  customerController.register
)
customerRouter.get(
  '/:customerId',
  [jwtAuth(jwtStrategyFn), validateCustomerIdInToken],
  customerController.getOneByIdParam
)
customerRouter.put(
  '/:customerId',
  [
    validateUserSchemaMiddleware,
    validateUserMongooseSchemaMiddleware,
    jwtAuth(jwtStrategyFn),
    validateCustomerIdInToken,
  ],
  customerController.updateOne
)

// Error handler
customerRouter.use(errorHandler)

module.exports = customerRouter
