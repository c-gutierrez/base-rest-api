const R = require('ramda')
const Ajv = require('ajv')
const ajv = new Ajv({ allErrors: true, useDefaults: true })
const { createSchemaObject, validateDocumentAgainstSchema } = require('../fn/mongooseModelFn')

/**
 * Validate the schema passed as parameter against request body
 * @param {Object} schema
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 */
const validateSchemaAgainstBody = schema => (req, res, next) => {
  const data = R.propOr({}, 'body', req)
  const validate = ajv.compile(schema)
  const valid = validate(data)

  if (!valid) {
    return next(validate.errors)
  }

  next()
}

/**
 * Validate the schema passed as parameter against request body
 * @param {Object} schema
 * @param {Object} req
 * @param {Object} res
 * @param {Function} next
 */
const validateSchemaMongooseAgainstBody = schema => (req, res, next) => {
  const data = R.propOr({}, 'body', req)
  validateDocumentAgainstSchema(createSchemaObject(schema), data)
    .then(() => next(null))
    .catch(next)
}

module.exports = { validateSchemaAgainstBody, validateSchemaMongooseAgainstBody }
