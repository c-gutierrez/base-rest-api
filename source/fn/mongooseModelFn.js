'use strict'

const R = require('ramda')
const mongoose = require('mongoose')
const Schema = mongoose.Schema
const Document = mongoose.Document

/**
 * Create a new instance of mongoose.Schema
 * @param {Object} dataSchema
 * @returns Object
 */
const createSchemaObject = dataSchema => new Schema(dataSchema, { timestamps: true })

/**
 * Create a new instance of mongoose.Schema
 * @param {Object} dataSchema
 * @returns Object
 */
const createSchemaObjectWithoutTimestamps = dataSchema => new Schema(dataSchema)

/**
 * Create a new mongoose object
 * @param {String} modelName
 * @param {Object} SchemaObject
 * @returns Object
 */
const createModelBySchemaObject = R.curry((modelName, SchemaObject) =>
  mongoose.model(modelName, SchemaObject)
)

/**
 * Create a new instance of mongoose.Schema and with it create a new mongoose model object
 * @param {String} modelName
 * @param {Object} SchemaObject
 * @returns Object
 */
const createCompleteModel = R.curry((modelName, dataSchema) =>
  mongoose.model(modelName, createSchemaObject(dataSchema))
)

const validateDocumentAgainstSchema = R.curry(
  (schema, document) =>
    new Promise((resolve, reject) => {
      const doc = new Document(document, schema)
      doc.validate(error => {
        if (error) return reject(error)
        return resolve({})
      })
    })
)

module.exports = {
  createSchemaObject,
  createSchemaObjectWithoutTimestamps,
  createModelBySchemaObject,
  createCompleteModel,
  validateDocumentAgainstSchema,
}
