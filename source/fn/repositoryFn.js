'use strict'

const R = require('ramda')

/**
 * This method either create a record or update it if already exists
 * @param {Object} dataObject Should be a Model instance to be saved or updated
 * @return {*}
 */
const saveFn = dataObject => dataObject.save()

/**
 * Create a new record in database
 * @param {Object} dataObject
 * @param {Object} model
 * @return Promise
 */
const createFn = R.curry((model, data) => {
  const newObj = new model()
  Object.assign(newObj, data)
  return newObj.save()
})

/**
 * Base object to retrieve info from a model
 * @param {Object} criteria
 * @param {String} fields
 * @param {Object} options
 * @param {Object} model
 * @return Promise
 */
const retrieveFn = model => (criteria, fields, options) =>
  model.find(criteria || {}, fields || {}, options || {})

/**
 * Base object to retrieve info from a model
 * @param {Object} criteria
 * @param {String} fields
 * @param {Object} options
 * @param {Object} model
 * @return Promise
 */
const retrieveOneFn = model => (criteria, fields, options) =>
  model.findOne(criteria || {}, fields || {}, options || {})

/**
 * Base object to retrieve info from a model by an id
 * @param {ObjectId} id
 * @param {String} fields
 * @param {Object} model
 * @return Promise
 */
const retrieveByIdFn = R.curry((model, id) => model.findOne({ _id: id }))

/**
 * Update all records according to coditions object and new values passed as parameters
 * @param {Object} model
 * @param {Object} data
 * @param {Object} conditions
 * @return Promise
 */
const updateFn = R.curry((model, data, conditions) => model.updateMany(conditions, data))

/**
 * Update all records according to coditions object and new values passed as parameters
 * @param {Object} model
 * @param {Object} data
 * @param {Object} conditions
 * @return Promise
 */
const updateOneFn = R.curry((model, data, conditions) =>
  model.findOneAndUpdate(conditions, data, { new: true })
)

/**
 * Delete all records according to conditions passed as parameters
 * @param {Object} model
 * @param {Object} conditions
 * @returns Object
 */
const deleteFn = R.curry((model, conditions) => model.deleteMany(conditions))

/**
 * Delete all records according to conditions passed as parameters
 * @param {Object} model
 * @param {Object} conditions
 * @returns Object
 */
const deleteOneFn = R.curry((model, conditions) => model.findOneAndRemove(conditions))

/**
 * Base object to count records inside a model
 * @param {Object} criteria
 * @param {Object} model
 * @return Object
 */
const countFn = R.curry((model, criteria, callback) => model.count(criteria, callback))

/**
 * Base object to count records inside a model and return it inside a promise
 * @param {Object} model
 * @param {Object} criteria
 * @return Promise
 */
const countPromiseFn = R.curry(
  (model, criteria) =>
    new Promise((resolve, reject) => {
      model.count(criteria, (error, success) => {
        if (error) {
          return reject(error)
        }
        return resolve(success)
      })
    })
)

/**
 * Base object to retrieve info from a model by an array of ids
 * @param {ObjectId} id
 * @param {Object} fields
 * @param {Object} model
 * @return Promise
 */
const retrieveByIdInArrayFn = R.curry((model, idsArray) => {
  const validateIsNotAnArray = R.compose(R.not, R.equals('Array'), R.type)
  if (validateIsNotAnArray(idsArray)) {
    return Promise.resolve([])
  }

  return model.find({ _id: { $in: idsArray } })
})

/**
 * Build a complete repository for crud actions
 * @param {Object} model
 * @param {Function} create
 * @param {Function} retrieve
 * @param {Function} update
 * @param {Function} remove
 * @return {{create: create, retrieve: baseRetrieveById, update: update, delete: delete}}
 */
const crudRepositoryFn = R.curry((model, create, retrieve, update, remove) => ({
  create: create(model),
  retrieve: retrieve(model),
  update: update(model),
  delete: remove(model),
}))

/**
 * Build a base object with minimal actions and behaviour
 * @param {Object} model
 * @return {{create: create, retrieve: baseRetrieveById, update: update, delete: delete}}
 */
const extendedRepositoryFn = model => {
  const repository = crudRepositoryFn(model, createFn, retrieveFn, updateFn, deleteFn)
  const extraFunctions = {
    save: saveFn,
    count: countFn(model),
    countPromise: countPromiseFn(model),
    retrieveOne: retrieveOneFn(model),
    retrieveById: retrieveByIdFn(model),
    retrieveByIdInArray: retrieveByIdInArrayFn(model),
    updateOne: updateOneFn(model),
    deleteOne: deleteOneFn(model),
  }

  return Object.assign(repository, extraFunctions)
}

module.exports = {
  saveFn,
  createFn,
  retrieveFn,
  retrieveOneFn,
  retrieveByIdFn,
  retrieveByIdInArrayFn,
  updateFn,
  updateOneFn,
  deleteFn,
  countFn,
  countPromiseFn,
  crudRepositoryFn,
  extendedRepositoryFn,
}
