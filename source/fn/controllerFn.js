'use strict'

const R = require('ramda')

/**
 * Receive a promise and its functions success and fail *
 * @param {Promise} promise
 * @param {Function} success
 * @param {Function} fail
 * @returns Promise
 */
const manageDatabasePromise = R.curry((promise, success, fail) => promise.then(success).catch(fail))

/**
 * Send a response to client
 * @param result
 * @param response
 */
const sendResponse = R.curry((response, result) => {
  response.status(R.propOr(500, ['status'], result))

  R.compose(
    R.forEach(item => {
      response.setHeader(item, result.headers[item])
    }),
    Object.keys
  )(R.propOr([], ['headers'], result))

  response.send(R.propOr({}, ['data'], result))
})

/**
 * Build an object to return a valid response
 * @param {Object} data
 * @returns {{status: number, headers: {Content-Type: string}, data: *}}
 */
const buildResponse = R.curry((status, data) => ({
  status,
  headers: { 'Content-Type': 'application/json' },
  data,
}))

/**
 * Parse Error object to a json to be returned in a response
 * @param {Object} error
 * @returns Object
 */
const parseErrorObject = error => {
  if (Array.isArray(error)) {
    return {
      errors: error,
    }
  }

  return {
    error: R.propOr('', ['message'], error),
    stack: R.propOr('', ['stack'], error),
  }
}

/**
 * Build and send a http response
 * @param {Object} response
 * @param {Number} status
 * @param {Object} data
 * @returns Object
 */
const completeResponse = R.curry((response, statusCode) =>
  R.compose(sendResponse(response), buildResponse(statusCode))
)

/**
 * Build and send a http error response
 * @param {Object} response
 * @param {Number} status
 * @param {Object} error
 * @returns Object
 */
const completeErrorResponse = R.curry((response, statusCode) =>
  R.compose(completeResponse(response, statusCode), parseErrorObject)
)

/**
 * Manage an occurred error
 * @param {Object} err
 * @param {Object} req
 * @param {Object} res
 * @param {Object} next
 * @returns Function
 */
const errorHandler = R.curry((err, req, res, next) => completeErrorResponse(res, 500, err))

/**
 * Retrieve all records and send them as http response
 * @param {Object} repository
 * @param {Object} req
 * @param {Object} res
 * @returns Promise
 */
const getAll = R.curry((repository, req, res) =>
  manageDatabasePromise(
    repository.retrieve(),
    completeResponse(res, 200),
    completeErrorResponse(res, 500)
  )
)

/**
 * Create a new record inside entity
 * @param {Object} repository
 * @param {Object} req
 * @param {Object} res
 * @returns Promise
 */
const createOne = repository => (req, res) => {
  const data = R.propOr([], 'body', req)
  return manageDatabasePromise(
    repository.create(data),
    completeResponse(res, 200),
    completeErrorResponse(res, 500)
  )
}

/**
 * Retrieve a record from the entity
 * @param {Object} repository
 * @param {Object} req
 * @param {Object} res
 * @returns Promise
 */
const getOne = R.curry((repository, req, res) =>
  manageDatabasePromise(
    repository.retrieveOne(),
    completeResponse(res, 200),
    completeErrorResponse(res, 500)
  )
)

/**
 * Retrieve a record by id from the entity
 * @param {Object} repository
 * @param {Object} req
 * @param {Object} res
 * @returns Promise
 */
const getOneByIdParam = R.curry((repository, req, res) => {
  const id = R.pathOr('', ['params', 'id'], req)
  return manageDatabasePromise(
    repository.retrieveOne({ _id: id }),
    completeResponse(res, 200),
    completeErrorResponse(res, 500)
  )
})

/**
 * Retrieve all records and send them as http response
 * @param {Object} repository
 * @param {Object} req
 * @param {Object} res
 * @returns Promise
 */
const getAllByFieldInParams = R.curry((repository, field, req, res) => {
  const criteria = {}
  criteria[field] = R.pathOr('', ['params', field], req)
  return manageDatabasePromise(
    repository.retrieve(criteria),
    completeResponse(res, 200),
    completeErrorResponse(res, 500)
  )
})

/**
 * Update a record by id from the entity
 * @param {Object} repository
 * @param {Object} req
 * @param {Object} res
 * @returns Promise
 */
const updateOneByIdParam = R.curry((repository, req, res) => {
  const id = R.pathOr('', ['params', 'id'], req)
  const data = R.propOr([], 'body', req)
  return manageDatabasePromise(
    repository.updateOne(data, { _id: id }),
    completeResponse(res, 200),
    completeErrorResponse(res, 500)
  )
})

/**
 * Retrieve a record by id from the entity
 * @param {Object} repository
 * @param {Object} req
 * @param {Object} res
 * @returns Promise
 */
const deleteOneByIdParam = R.curry((repository, req, res) => {
  const id = R.pathOr('', ['params', 'id'], req)
  return manageDatabasePromise(
    repository.deleteOne({ _id: id }),
    completeResponse(res, 200),
    completeErrorResponse(res, 500)
  )
})

/**
 * Create an object with the basic operations
 * @param {Object} repository
 * @returns {Object}
 */
const crudControllerFn = R.curry(repository => ({
  getAll: getAll(repository),
  createOne: createOne(repository),
  getOne: getOne(repository),
  updateOne: updateOneByIdParam(repository),
  deleteOne: deleteOneByIdParam(repository),
}))

/**
 * Build a base object with minimal actions and behaviour
 * @param {Object} model
 * @return {{create: create, retrieve: baseRetrieveById, update: update, delete: delete}}
 */
const extendedControllerFn = repository => {
  const controller = crudControllerFn(repository)
  const extraFunctions = {
    errorHandler,
    getOneByIdParam: getOneByIdParam(repository),
    getAllByFieldInParams: getAllByFieldInParams(repository),
    updateOneByIdParam: updateOneByIdParam(repository),
    deleteOneByIdParam: deleteOneByIdParam(repository),
  }

  return Object.assign(controller, extraFunctions)
}

module.exports = {
  manageDatabasePromise,
  sendResponse,
  buildResponse,
  completeResponse,
  completeErrorResponse,
  parseErrorObject,
  errorHandler,
  getAll,
  createOne,
  getOne,
  getOneByIdParam,
  getAllByFieldInParams,
  updateOne: updateOneByIdParam,
  updateOneByIdParam,
  deleteOne: deleteOneByIdParam,
  deleteOneByIdParam,
  crudControllerFn,
  extendedControllerFn,
}
