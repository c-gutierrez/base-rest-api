'use strict'

const R = require('ramda')

/**
 * Run toObject function from an object
 * @param {Object} obj
 * @returns Object
 */
const toObject = obj => (obj ? obj.toObject() : {})

/**
 * Print a value and return it
 * @param {Object} data
 * @return Object
 */
const debugFn = data => console.info(data) || data

/**
 * Take a property from an object and return its value or default value
 * @param {Object} object
 * @param {Mixed} object
 * @param {String} object
 * @return Mixed
 */
const propOrDefaultFrom = R.curry((object, defaultValue, property) =>
  R.propOr(defaultValue, [property], object)
)

module.exports = {
  debugFn,
  toObject,
  propOrDefaultFrom,
}
