'use strict'

const mongoose = require('mongoose')
const R = require('Ramda')

/**
 * Create a new connection to database
 * @param {Object} connectionParams
 */
const connect = R.curry(connectionParams => {
  const { host, port, dbName, user, pass } = connectionParams

  mongoose.connection
    .on('error', error => {
      console.error('mongodb connection error: ', error)
      // If mongoerror is emitted, process.exit(1) is launched to stop application
      console.info('process.exit(1) launched to stop application')
      process.exit(1)
    })
    .on('connecting', () => console.info('mongodb connecting'))
    .on('connected', () => console.info('mongodb connected'))
    .on('disconnected', () => console.info('mongodb disconnected'))
    .on('reconnected', () => console.info('mongodb reconnected'))
    .on('reconnectFailed', () => {
      console.error('mongodb reconnectFailed')
      // If reconnect process fail, process.exit(1) is launched to stop application
      console.info('process.exit(1) launched to stop application')
      process.exit(1)
    })
    .on('disconnecting', () => console.info('mongodb disconnecting'))
    .on('open', () => console.info('mongodb open'))
    .on('close', () => console.info('mongodb close'))

  return mongoose.connect(`mongodb://${host}:${port}/${dbName}`, {
    user,
    pass,
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    autoIndex: false,
    useFindAndModify: false,
  })
})

/**
 * Disconnect from mongoose database connection
 */
const disconnect = () => mongoose.disconnect()

module.exports = {
  connect,
  disconnect,
}
