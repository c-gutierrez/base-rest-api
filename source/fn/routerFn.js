const R = require('ramda')

/**
 * Assign the middelware objects to a router
 * @param {Object} router
 * @param {Object} middlewares
 * @return Object
 */
const assignMiddlewaresToRouter = (router, middlewares = []) => {
  const validateIsNotAnArray = R.compose(R.not, R.equals('Array'), R.type)

  if (validateIsNotAnArray(middlewares) || R.isEmpty(middlewares)) {
    return router
  }

  router.use(middlewares)
  return router
}

/**
 * Create a REST routers actions
 * @param {Object} router
 * @param {Object} controller
 */
const createBasicRoutes = R.curry((router, controller) => {
  router
    .route('/')
    .get(controller.getAll)
    .post(controller.createOne)

  router
    .route('/:id')
    .get(controller.getOneByIdParam)
    .put(controller.updateOneByIdParam)
    .delete(controller.deleteOneByIdParam)

  router.all('*', (req, res) => {
    res.status(404).json({ error: 'Route not Found' })
  })

  return router
})

module.exports = {
  createBasicRoutes,
  assignMiddlewaresToRouter,
}
