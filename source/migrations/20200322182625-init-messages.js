module.exports = {
  up(db, client) {
    return db
      .createCollection('messages')
      .then(collection =>
        Promise.all([
          collection.createIndex({ 'recipients.contactInfo.$**': 1 }),
          collection.createIndex({ customerId: 1 }),
        ])
      )
      .catch(error => {
        console.error(
          `The index {recipients.contactInfo.$**: 1} in messages collection could not be created`
        )
        console.error(`The index {customerId: 1} in messages collection could not be created`)
        console.error(error)
        throw error
      })
  },
  down(db, client) {
    return Promise.all([
      db.collection('messages').dropIndex({ 'recipients.contactInfo.$**': 1 }),
      db.collection('messages').dropIndex({ customerId: 1 }),
    ]).catch(error => {
      console.error(
        `The index {recipients.contactInfo.$**: 1} in messages collection could not be removed`
      )
      console.error(`The index {customerId: 1} in messages collection could not be removed`)
      console.error(error)
      throw error
    })
  },
}
