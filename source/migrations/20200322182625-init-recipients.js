module.exports = {
  up(db, client) {
    return db
      .createCollection('recipients')
      .then(collection =>
        Promise.all([
          collection.createIndex({ 'contactInfo.$**': 1 }),
          collection.createIndex({ customerId: 1 }),
        ])
      )
      .catch(error => {
        console.error(
          `The index {contactInfo.$**: 1} in recipients collection could not be created`
        )
        console.error(`The index {customerId: 1} in recipients collection could not be created`)
        console.error(error)
        throw error
      })
  },
  down(db, client) {
    return Promise.all([
      db.collection('recipients').dropIndex({ 'contactInfo.$**': 1 }),
      db.collection('recipients').dropIndex({ customerId: 1 }),
    ]).catch(error => {
      console.error(`The index {contactInfo.$**: 1} in recipients collection could not be removed`)
      console.error(`The index {customerId: 1} in recipients collection could not be removed`)
      console.error(error)
      throw error
    })
  },
}
