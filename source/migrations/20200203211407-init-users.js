module.exports = {
  up(db, client) {
    return db
      .createCollection('users')
      .then(collection => collection.createIndex({ email: 1 }, { sparse: true, unique: true }))
      .catch(error => {
        console.error(`The index {email: 1} in users collection could not be created`)
        console.error(error)
        throw error
      })
  },
  down(db, client) {
    return db
      .collection('users')
      .dropIndex({ email: 1 })
      .catch(error => {
        console.error(`The index {email: 1} in users collection could not be removed`)
        console.error(error)
        throw error
      })
  },
}
