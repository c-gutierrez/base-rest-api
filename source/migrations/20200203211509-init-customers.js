module.exports = {
  up(db, client) {
    return db
      .createCollection('customers')
      .then(collection => collection.createIndex({ email: 1 }, { sparse: true, unique: true }))
      .catch(error => {
        console.error(`The index {email: 1} in customers collection could not be created`)
        console.error(error)
        throw error
      })
  },
  down(db, client) {
    return db
      .collection('customers')
      .dropIndex({ email: 1 })
      .catch(error => {
        console.error(`The index {email: 1} in customers collection could not be removed`)
        console.error(error)
        throw error
      })
  },
}
