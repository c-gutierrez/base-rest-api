'use strict'

const crypto = require('crypto')
const fs = require('fs')
const R = require('ramda')
const nodemailer = require('nodemailer')
const { propOrDefaultFrom, decryptValueWithCrypto } = require('../fn/helperFn')

/**
 * Create a file with the data passed as parameter
 * @param {String} path
 * @param {String} data
 * @returns Promise
 */
const createMessagePasswordFile = R.curry((path, data) => {
  const name = crypto.randomBytes(20).toString('hex')

  return new Promise((resolve, reject) => {
    fs.writeFile(`${path}${name}`, data, error => {
      if (error) reject(error)
      resolve(name)
    })
  })
})

/**
 * Read a message password file and return its data
 * @param {String} path
 * @param {String} data
 * @returns Promise
 */
const retrieveMessagePasswordFromFile = R.curry(
  (path, fileName) =>
    new Promise((resolve, reject) => {
      fs.readFile(`${path}${fileName}`, 'utf-8', (error, data) => {
        if (error) reject(error)
        resolve(data)
      })
    })
)

/**
 * Validate and return a plain message from a password in a file
 * @param {Object} config
 * @param {Object} messageRecord
 * @return Promise(String)
 */
const decryptMessage = R.curry((config, messageRecord) => {
  const propOrFromMessage = propOrDefaultFrom(messageRecord)
  const propOrFromConfig = propOrDefaultFrom(config)

  const encrypted = propOrFromMessage(false, 'encrypted')

  if (!encrypted) {
    return propOrFromMessage('', 'message')
  }

  const message = propOrFromMessage(false, 'message')
  const messagePasswordFileName = propOrFromMessage(false, 'messagePassword')

  return retrieveMessagePasswordFromFile(
    propOrFromConfig('', 'messagesPasswordsFilesPath'),
    messagePasswordFileName
  ).then(decryptValueWithCrypto(message))
})

/**
 * Create an SMTP transport from config object to send emails
 * @param {Object} config
 * @returns Object
 */
const createSMTPTransportByConfig = config => {
  const propOrFromConfig = propOrDefaultFrom(config)

  return nodemailer.createTransport({
    host: propOrFromConfig('', 'host'),
    port: propOrFromConfig('', 'port'),
    auth: {
      user: propOrFromConfig('', 'user'),
      pass: propOrFromConfig('', 'pass'),
    },
    secureConnection: false,
    logger: propOrFromConfig(false, 'logger'),
    debug: propOrFromConfig(false, 'debug'),
  })
}

/**
 * Send an email using transport and data passed as parameters
 * @param {Object} transport
 * @param {String} sender
 * @param {String} sender
 * @param {String} recipient
 * @param {String} message
 * @returns Promise
 */
const sendMessageByEmailToRecipient = R.curry(
  (transport, sender, subject, message, recipient) =>
    new Promise((resolve, reject) => {
      transport.sendMail(
        {
          subject,
          from: sender,
          to: recipient,
          html: message,
        },
        (error, info) => {
          if (error) {
            return reject(error)
          }
          return resolve(info)
        }
      )
    })
)

/**
 * Send an email using transport and data passed as parameters to many recipients
 * @param {Object} transport
 * @param {String} sender
 * @param {String} sender
 * @param {String} recipient
 * @param {String} message
 * @returns Promise
 */
const sendMessageByEmailToRecipients = R.curry((transport, sender, subject, message) =>
  R.map(sendMessageByEmailToRecipient(transport, sender, subject, message))
)

module.exports = {
  createMessagePasswordFile,
  retrieveMessagePasswordFromFile,
  decryptMessage,
  createSMTPTransportByConfig,
  sendMessageByEmailToRecipient,
  sendMessageByEmailToRecipients,
}
