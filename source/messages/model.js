'use strict'

const { createCompleteModel } = require('../fn/mongooseModelFn')
const schema = require('./mongooseSchema')

module.exports = createCompleteModel('messages', schema)
