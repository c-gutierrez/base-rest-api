'use strict'

const R = require('ramda')
const express = require('express')
const messageRouter = express.Router({ mergeParams: true })
const { assignMiddlewaresToRouter, createBasicRoutes } = require('../fn/routerFn')
const { errorHandler } = require('../fn/controllerFn')
const { messageControllerFn } = require('./controller')
const { messageRepositoryFn } = require('./repository')
const {
  validateSchemaMongooseAgainstBody,
  validateSchemaAgainstBody,
} = require('../fn/middlewareFn')
const {
  validateCustomerIdInToken,
  injectCustomerIdInBodyFromParam,
} = require('../customers/middlewareFn')
const buildControllerAndRepoByModel = R.compose(messageControllerFn, messageRepositoryFn)
const messageModel = require('./model')
const messageController = buildControllerAndRepoByModel(messageModel)
const messageMongooseSchema = require('./mongooseSchema')
const validateMessageMongooseSchemaMiddleware = validateSchemaMongooseAgainstBody(
  messageMongooseSchema
)
const messageSchema = require('./schema')
const validateMessageSchemaMiddleware = validateSchemaAgainstBody(messageSchema)
const { jwtAuth } = require('../passport/auth')
const jwtStrategyFn = require('../customers/jwtStrategy')

assignMiddlewaresToRouter(messageRouter, [
  jwtAuth(jwtStrategyFn),
  validateCustomerIdInToken,
  injectCustomerIdInBodyFromParam,
])

messageRouter.get('/', messageController.getAllByFieldInParams('customerId'))
messageRouter.post('/', [validateMessageMongooseSchemaMiddleware, validateMessageSchemaMiddleware])
messageRouter.put('/:id', [
  validateMessageMongooseSchemaMiddleware,
  validateMessageSchemaMiddleware,
])
messageRouter.post('/:id/decrypt', messageController.decrypt)

createBasicRoutes(messageRouter, messageController)

// Error handler
messageRouter.use(errorHandler)

module.exports = messageRouter
