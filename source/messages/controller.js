'use strict'

const R = require('ramda')
const {
  extendedControllerFn,
  manageDatabasePromise,
  completeResponse,
  completeErrorResponse,
  createOne: createOneGeneral,
  updateOneByIdParam: updateOneByIdParamGeneral,
} = require('../fn/controllerFn')
const config = require('../config')

/**
 * Add the messagesPasswordsPath parameter to create a record message
 * @param {Object} repository
 * @param {Object} req
 * @param {Object} res
 * @returns Promise
 */
const createOne = repository => (req, res) => {
  req.body.messagesPasswordsFilesPath = R.propOr('./', ['messagesPasswordsFilesPath'], config)
  return createOneGeneral(repository)(req, res)
}

/**
 * Add the messagesPasswordsPath parameter to create a record message
 * @param {Object} repository
 * @param {Object} req
 * @param {Object} res
 * @returns Promise
 */
const updateOneByIdParam = repository => (req, res) => {
  req.body.messagesPasswordsFilesPath = R.propOr('./', ['messagesPasswordsFilesPath'], config)
  return updateOneByIdParamGeneral(repository, req, res)
}

/**
 * Decrypt a message with a password passed as parameter
 * @param {Function} retrieveByIdFn
 * @param {Object} req
 * @param {Object} res
 * @returns Promise
 */
const decrypt = decryptMessage => (req, res) => {
  const messagePassword = R.pathOr('', ['body', 'messagePassword'], req)
  const messageId = R.pathOr('', ['params', 'id'], req)

  return manageDatabasePromise(
    decryptMessage(messageId, messagePassword),
    R.compose(completeResponse(res, 200), message => ({ message })),
    completeErrorResponse(res, 500)
  )
}

/**
 * Create a new object with the crud and extra functions
 * @param {Object} repository
 * @returns {}
 */
const messageControllerFn = repository => {
  const controller = extendedControllerFn(repository)
  const extraFunctions = {
    createOne: createOne(repository),
    updateOneByIdParam: updateOneByIdParam(repository),
    decrypt: decrypt(repository.decryptMessage),
  }

  return Object.assign(controller, extraFunctions)
}

module.exports = { messageControllerFn }
