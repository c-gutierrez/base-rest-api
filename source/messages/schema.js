'use strict'

const schema = {
  properties: {
    customerId: { type: 'string' },
    recipients: {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          name: { type: 'string' },
          relationship: { type: 'string' },
          contactInfo: {
            type: 'object',
            properties: {
              email: { type: 'string', format: 'email' },
            },
          },
        },
      },
    },
    message: { type: 'string' },
    encrypted: { type: 'boolean', default: false },
    messagePassword: { type: 'string' },
  },
  required: ['customerId', 'recipients', 'message'],
  allOf: [
    {
      if: { properties: { encrypted: { enum: [true] } } },
      then: { required: ['messagePassword'] },
    },
  ],
}

module.exports = schema
