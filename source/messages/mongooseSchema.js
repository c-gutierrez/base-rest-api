'use strict'

const R = require('ramda')
const mongoose = require('mongoose')
const { createSchemaObjectWithoutTimestamps } = require('../fn/mongooseModelFn')
const recipientSchema = require('../recipients/mongooseSchema')
const schemaObjectWithoutCustomerIdFn = R.compose(
  createSchemaObjectWithoutTimestamps,
  R.assoc('_id', false),
  R.pick(['name', 'relationship', 'contactInfo'])
)

const schema = {
  customerId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'customers',
    index: true,
  },
  recipients: [schemaObjectWithoutCustomerIdFn(recipientSchema)],
  message: {
    type: String,
    required: true,
  },
  encrypted: {
    type: Boolean,
    default: false,
  },
  messagePassword: {
    type: String,
    required: function() {
      return this.encrypted === true
    },
  },
}

module.exports = schema
