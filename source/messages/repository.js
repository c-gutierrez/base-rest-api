'use strict'

const R = require('ramda')
const {
  extendedRepositoryFn,
  createFn,
  updateOneFn: updateOneGeneralFn,
} = require('../fn/repositoryFn')
const {
  createMD5Hash,
  encryptValueWithMD5HashKey,
  decryptValueWithMD5HashKey,
} = require('../fn/encryptionFn')

const { createMessagePasswordFile } = require('./fn')

/**
 * Create a new record inside entity and save the message password file
 * if it's nedeed
 * @param {Object} repository
 * @param {Object} req
 * @param {Object} res
 * @returns Promise
 */
const createOneFn = model => data => {
  const encrypted = R.pathOr(false, ['encrypted'], data)

  if (!encrypted) {
    return createFn(model, data)
  }

  const message = R.pathOr('', ['message'], data)
  const messagePassword = R.pathOr('', ['messagePassword'], data)
  const messagesPasswordsFilesPath = R.pathOr('', ['messagesPasswordsFilesPath'], data)

  return R.compose(
    createMessagePasswordFile(messagesPasswordsFilesPath),
    createMD5Hash
  )(messagePassword).then(fileMessagePassword => {
    data.message = encryptValueWithMD5HashKey(message)(messagePassword)
    data.messagePassword = fileMessagePassword

    return createFn(model, data)
  })
}

/**
 * Update a record inside entity and save a new message password file
 * if it's nedeed
 * @param {Object} repository
 * @param {Object} req
 * @param {Object} res
 * @returns Promise
 */
const updateOneFn = R.curry((model, data, conditions) => {
  const encrypted = R.pathOr(false, ['encrypted'], data)

  if (!encrypted) {
    return updateOneGeneralFn(model, data, conditions)
  }

  const message = R.pathOr('', ['message'], data)
  const messagePassword = R.pathOr('', ['messagePassword'], data)
  const messagesPasswordsFilesPath = R.pathOr('', ['messagesPasswordsFilesPath'], data)

  return R.compose(
    createMessagePasswordFile(messagesPasswordsFilesPath),
    createMD5Hash
  )(messagePassword).then(fileMessagePassword => {
    data.message = encryptValueWithMD5HashKey(message)(messagePassword)
    data.messagePassword = fileMessagePassword

    return updateOneGeneralFn(model, data, conditions)
  })
})

/**
 * Decrypt a message with a password passed as parameter
 * @param {Function} retrieveByIdFn
 * @param {Object} req
 * @param {Object} res
 * @returns Promise
 */
const decryptMessageFn = R.curry((retrieveByIdFn, messageId, messagePassword) =>
  retrieveByIdFn(messageId).then(
    R.compose(decryptValueWithMD5HashKey(R.__, messagePassword), R.propOr('', 'message'))
  )
)

/**
 * Create a new object with the crud and extra functions
 * @param {Object} repository
 * @returns {}
 */
const messageRepositoryFn = model => {
  const controller = extendedRepositoryFn(model)
  const extraFunctions = {
    create: createOneFn(model), // Overwrite create function to create message password file if it's needed
    updateOne: updateOneFn(model), // Overwrite create function to create message password file if it's needed
    decryptMessage: decryptMessageFn(model.retrieveById),
  }

  return Object.assign(controller, extraFunctions)
}

module.exports = { messageRepositoryFn }
