const configParams = require('./config')
const R = require('ramda')
const connectionParams = R.propOr({}, ['db'], configParams)
const { host, port, dbName, user, pass } = connectionParams

const config = {
  mongodb: {
    url: `mongodb://${host}:${port}`,
    databaseName: dbName,
    options: {
      user,
      password: pass,
      useNewUrlParser: true,
      useUnifiedTopology: true,
    },
  },

  // The migrations dir, can be an relative or absolute path. Only edit this when really necessary.
  migrationsDir: './migrations',

  // The mongodb collection where the applied changes are stored. Only edit this when really necessary.
  changelogCollectionName: 'migrationsChangelog',
}

// Return the config as a promise
module.exports = config
